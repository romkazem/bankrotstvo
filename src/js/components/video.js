(function () {
  var video = document.querySelector('.modal--video');
  var triggerVideo = document.querySelector('.section-1__buttom-video');
  if (video !== null) {
    var closeVideoButton = video.querySelector('.modal__close-button');

    var iframe = video.querySelector('.youtube-player > iframe');
    var srcIframe = iframe.getAttribute('src');


    function toggleVideo() {
      video.classList.toggle('modal--show');
      if (!video.classList.contains('modal--show')) {
        iframe.src = srcIframe;
      } else {
        iframe.src = srcIframe + "?autoplay=1";
      }
    }

    function windowOnClick(event) {
      if (event.target === video) {
        toggleVideo();
      }
    }

    triggerVideo.addEventListener('click', function (event) {
      event.preventDefault();
      toggleVideo();
    });
    closeVideoButton.addEventListener('click', toggleVideo);
    window.addEventListener('click', windowOnClick);
    document.addEventListener('keyup', function (e) {
      if (e.keyCode == 27 && video.classList.contains('modal--show')) {
        toggleVideo();
      }
    });
  }
})();