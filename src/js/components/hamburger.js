(function () {
  var menuShowing = false;
  var scrollPosition = 0;
  var hamburger = $('.hamburger');
  var pageHeader = $('.page-header');
  var mainMenu = $('.main-nav');

  document.querySelector('.hamburger').addEventListener('click', function () {
    if (!menuShowing) {
      openMenu();
    } else {
      closeMenu();
    }

    menuShowing = !menuShowing;
  });

  var openMenu = function () {
    $(hamburger).addClass('hamburger--active');
    $(pageHeader).addClass('page-header--full-width');
    $(mainMenu).addClass('main-nav--show');
    scrollPosition = window.pageYOffset;
    var mainEl = document.querySelector('body');
    mainEl.style.top = -scrollPosition + 'px';
    document.body.classList.add('no-scroll');
  }

  var closeMenu = function (afterTapHamburger) {
    if (afterTapHamburger !== undefined) {
      afterTapHamburger = afterTapHamburger;
      menuShowing = !menuShowing;
    } else {
      afterTapHamburger = true;
    }

    $(hamburger).removeClass('hamburger--active');
    $(pageHeader).removeClass('page-header--full-width');
    $(mainMenu).removeClass('main-nav--show');

    document.body.classList.remove('no-scroll');
    window.scrollTo(0, scrollPosition);
  }


  window.humburger = {
    closeMenu: closeMenu
  };
})();
