(function () {
  var tabsButtonsQuestions = document.querySelectorAll('.questions .questions__btn');
  var question = document.querySelector('.questions');
  var questionHeight = question.offsetHeight;

  function getQuestionHeight(element) {
    var elementHeight = element.offsetHeight;

    if (elementHeight > questionHeight) {
      questionHeight = elementHeight;
    }

    return questionHeight;
  }


  function closeAllTabs(buttons) {
    buttons.forEach(function (item, i, arr) {
      var questionDescription = document.getElementById(item.dataset.content);
      if (questionDescription.classList.contains('questions--show')) {
        item.classList.remove('questions__btn--active');
        questionDescription.classList.remove('questions--show');
      }
    });
  }

  function questionButtonsClickHandler(el, questionButtons) {
    var questionItem = el.closest('.questions__item');
    var questionDescription = document.getElementById(el.dataset.content);

    if (questionDescription.classList.contains("questions--show")) {
      questionDescription.classList.remove('questions--show');
      el.classList.remove('questions__btn--active');
    } else {
      closeAllTabs(questionButtons);
      el.classList.add('questions__btn--active');
      el.scrollIntoView({
        behavior: "smooth"
      });
      questionDescription.classList.add('questions--show');
    }

    question.style.height = getQuestionHeight(question) + 'px';
  }

  for (var i = 0; i < tabsButtonsQuestions.length; i++) {
    var btn = tabsButtonsQuestions[i];

    btn.addEventListener('click', function (event) {
      event.preventDefault();
      questionButtonsClickHandler(this, tabsButtonsQuestions);
    });
  }
})();