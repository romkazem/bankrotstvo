(function () {
  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }
    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var Switch = function () {
    function Switch(selector) {
      var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$highlightClass = _ref.highlightClass,
        highlightClass = _ref$highlightClass === undefined ? "switch__highlight" : _ref$highlightClass,
        _ref$activeClass = _ref.activeClass,
        activeClass = _ref$activeClass === undefined ? "is-active" : _ref$activeClass;

      _classCallCheck(this, Switch);

      this.activeClass = activeClass;
      this.element = document.querySelector(selector);
      this.buttons = this.element.querySelectorAll("button");
      this.highlight = this.element.querySelector("." + highlightClass);
      this.activeBtn = this.element.querySelector("button." + this.activeClass);

      if (!this.activeBtn) {
        this.activeBtn = this.buttons[0];
        this.activeBtn.classList.add(this.activeClass);
        var activeCard = document.getElementById(this.activeBtn.dataset.value);
        activeCard.classList.add('landing-form__item--show');
      }

      this._highlight();
      this._addEvents();
    }

    _createClass(Switch, [{
      key: "_highlight",
      value: function _highlight() {
        var w = this.activeBtn.offsetWidth;
        var x = this.activeBtn.offsetLeft;

        this.highlight.style.width = w + "px";
        this.highlight.style.transform = "translateX(" + x + "px)";
      }
    }, {
      key: "_dispatchEvent",
      value: function _dispatchEvent() {
        this.element.dispatchEvent(new CustomEvent("change", {
          detail: this.activeBtn.dataset.value
        }));
      }
    }, {
      key: "_addEvents",
      value: function _addEvents() {
        var _this = this;

        [].forEach.call(this.buttons, function (btn) {
          return btn.addEventListener("click", function (e) {
            if (_this.activeBtn === e.target) return;
            var activeCard = document.getElementById(_this.activeBtn.dataset.value);
            activeCard.classList.remove('landing-form__item--show');
            _this.activeBtn.classList.remove(_this.activeClass);
            _this.activeBtn = e.target;
            _this.activeBtn.classList.add(_this.activeClass);
            var activeCard = document.getElementById(_this.activeBtn.dataset.value);
            activeCard.classList.add('landing-form__item--show');

            _this._highlight();
            _this._dispatchEvent();
          });
        });
      }
    }]);

    return Switch;
  }();

  var mySwitch = new Switch(".switch");
})();