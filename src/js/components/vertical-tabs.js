(function () {
  var formatButtons = document.querySelectorAll('.format .list__btn');
  var formatListItems = document.querySelectorAll('.format .list__item');
  console.log(formatButtons)

  var DESKTOP_WIDTH = 1025;
  var widthWindow = window.innerWidth || document.body.clientWidth;

  function isMobile(currentWidth, desktopWidth) {
    return (currentWidth < desktopWidth) ? true : false;
  }

  function removeListItemClass(items) {
    items.forEach(function (item, i, arr) {
      if (item.classList.contains('list__item--without-borders') && isMobile(widthWindow, DESKTOP_WIDTH)) {
        item.classList.remove('list__item--without-borders')
      }
    });
  }

  function closeAllTabs(buttons) {
    buttons.forEach(function (item, i, arr) {
      var buttonCard = document.getElementById(item.dataset.content);
      if (buttonCard.classList.contains('card--show')) {
        var listItem = item.closest('.list__item');
        var listAdditionalInfo = listItem.querySelector('.list__additional-info');
        if (listAdditionalInfo !== null) {
          listAdditionalInfo.classList.remove('list__additional-info--active');
        }
        item.classList.remove('list__btn--active');
        buttonCard.classList.remove('card--show');
      }
    });
  }

  function tabClickHandler(el, tabButtons, listItems) {
    var listItem = el.closest('.list__item');
    var card = document.getElementById(el.dataset.content);

    if (card.classList.contains("card--show") && isMobile(widthWindow, DESKTOP_WIDTH)) {
      card.classList.remove('card--show');
      el.classList.remove('list__btn--active');
      removeListItemClass(listItems);
    } else {
      var listAdditionalInfo = listItem.querySelector('.list__additional-info');
      removeListItemClass(listItems);
      closeAllTabs(tabButtons);
      if (listAdditionalInfo !== null) {
        listAdditionalInfo.classList.add('list__additional-info--active');
      }
      listItem.classList.add('list__item--without-borders');
      el.classList.add('list__btn--active');
      card.classList.add('card--show');

      // if (isMobile(widthWindow, DESKTOP_WIDTH)) {
      //   listItem.scrollIntoView({
      //     behavior: 'smooth'
      //   });
      // }
    }
  }

  for (var i = 0; i < formatButtons.length; i++) {
    var btn = formatButtons[i];

    btn.addEventListener('click', function (event) {
      event.preventDefault();
      tabClickHandler(this, formatButtons, formatListItems);

    });
  }

})();
