(function () {
  var modal = document.querySelector(".modal--questions");
  if (modal !== null) {
    var triggerModal = document.querySelector(".modal__button");
    var closeModalButton = modal.querySelector(".modal__close-button");
    var btns = modal.querySelectorAll('[data-content]');

    function toggleModal() {
      closeAllPopups(btns);
      modal.classList.toggle("modal--show");
    }

    function windowOnClick(event) {
      if (event.target === modal) {
        toggleModal();
      }
    }

    function closeAllPopups(buttons) {
      buttons.forEach(function (item, i, arr) {
        var buttonPopup = document.getElementById(item.dataset.content);
        if (buttonPopup.classList.contains('modal__question-text--show')) {
          buttonPopup.classList.remove('modal__question-text--show');
        }
      });
    }

    document.addEventListener('keyup', function (e) {
      if (e.keyCode == 27 && modal.classList.contains('modal--show')) {
        toggleModal();
      }
    });

    triggerModal.addEventListener("click", toggleModal);
    closeModalButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);

    for (var i = 0; i < btns.length; i++) {
      var btn = btns[i];

      btn.addEventListener("click", function (event) {
        event.preventDefault();
        closeAllPopups(btns);
        var popup = document.getElementById(this.dataset.content);
        popup.classList.add('modal__question-text--show');
      });
    }
  }
})();