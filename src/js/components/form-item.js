(function () {
  var allCourseCard = document.querySelector(".form-item--all");
  var partialCourseCard = document.querySelector(".form-item--partial");
  var allCourseBtn = document.querySelector("#btn-all");
  var partialCourseBtn = document.querySelector("#btn-partial");

  allCourseBtn.addEventListener("click", function (event) {
    event.preventDefault();
    allCourseCard.classList.add("form-item--tablet-hide");
    partialCourseCard.classList.remove("form-item--tablet-hide");
  });

  partialCourseBtn.addEventListener("click", function (event) {
    event.preventDefault();
    partialCourseCard.classList.add("form-item--tablet-hide");
    allCourseCard.classList.remove("form-item--tablet-hide");
  });
})();