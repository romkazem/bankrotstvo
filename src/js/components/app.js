(function () {
  var pageHeader = $('.page-header');

  var introSection = $('.intro');
  var DESKTOP_WIDTH = 1025;
  var widthWindow = window.innerWidth || document.body.clientWidth;
  var modalOverlay = $('.modal__overlay');
  var transitionTime = 250;
  var openModal;

  $('.accordion').click(function () {
    $(this).find('.accordion__inner').toggleClass('selected');
    $(this).find('.accordion__content').slideToggle(250);
  });

  $('.authors__wrapper').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-author');
    openModal = $('#' + id);
    openModal.fadeIn(transitionTime);
    modalOverlay.fadeIn(transitionTime);
  });

  $(modalOverlay).click(function (e) {
    e.preventDefault();
    openModal.hide();
    modalOverlay.hide();
  });

  $('.modal__close-button').click(function (e) {
    e.preventDefault();
    $(this).parents('.modal').eq(0).hide();
    modalOverlay.hide();
  });

  $(document).keyup(function (e) {
    if (e.keyCode == 27) {
      $('.modal').hide()
      modalOverlay.hide();
    }
  });

  if (widthWindow >= DESKTOP_WIDTH) {
    $(".main-nav a").click(function (e) {
      e.preventDefault();
      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top - 100
      }, 'slow');
    });

    $(window).scroll(function () {
      if ($(window).scrollTop() > 0) {
        $(pageHeader).addClass('page-header__fixed-top');
        $(introSection).addClass('intro--header-fixed');
      } else {
        $(pageHeader).removeClass('page-header__fixed-top');
        $(introSection).removeClass('intro--header-fixed');
      }
    });

  } else {
    $(".main-nav a").click(function (e) {
      e.preventDefault();
      window.humburger.closeMenu(false);
      $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top - 60
      }, 'slow');
    });
  }
})();
