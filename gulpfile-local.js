"use strict";

var config = {
    srcPath : {
        sass: 'src/style/**/*.s*ss',
    },

    destPath : {
        src_css: '../../../src/main/webapp/promo/promo5/css',
        tomcat_css: '../../../target/legalacademy/promo/promo5/css',
    }
};


var gulp = require("gulp");
var sass = require("gulp-sass");
var rigger = require('gulp-rigger');
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var server = require("browser-sync").create();
var csso = require("gulp-csso");
var del = require("del");
var imagemin = require("gulp-imagemin");
var posthtml = require("gulp-posthtml");
var include = require("posthtml-include");
var pump = require("pump");
var rename = require("gulp-rename");
var run = require("run-sequence");
var svgstore = require("gulp-svgstore");
var svgmin = require("gulp-svgmin");
var uglify = require("gulp-uglify");
var webp = require("gulp-webp");

//Автопрефиксер и минификация
gulp.task("style", function() {
  gulp.src("src/style/main.sass")
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest("build/css"))
    .pipe(csso())
    .pipe(rename("main.min.css"))
    .pipe(gulp.dest("build/css"))
    .pipe(server.stream());
});

//Минификация JS
gulp.task("js", function (cb) {
    return gulp.src(["src/js/*.js"])
    .pipe(rigger())
    .pipe(uglify())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest("build/js"));
});

//Оптимизация изображений
gulp.task("images", function () {
  return gulp.src("build/img/**/*.{png,jpg,svg}")
  .pipe(imagemin([
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 3}),
    imagemin.svgo()
  ]))
  .pipe(gulp.dest("build/img"));
});

//Конвертация в webp
gulp.task("webp", function () {
  return gulp.src("build/img/**/*.{png,jpg}")
  .pipe(webp({quality: 80}))
  .pipe(gulp.dest("build/img"));
});

//SVG спрайт
gulp.task("sprite", function () {
  return gulp.src("src/img/icons/*.svg")
  .pipe(svgmin({
    plugins: [{
      removeAttrs: {attrs: 'fill'}
    }]
  }))
  .pipe(svgstore({
    inlineSvg: true
  }))
  .pipe(rename("sprite.svg"))
  .pipe(gulp.dest("build/img"));
});

//posthtml-include
gulp.task("html", function () {
  return gulp.src("src/*.html")
  .pipe(rigger())
  .pipe(gulp.dest("build"));
});

//Очистка build
gulp.task("clean", function () {
  return del("build");
});

//Копирование в build
gulp.task("copyfont", function () {
  return gulp.src([
    "src/fonts/**/*.{woff,woff2}"
  ])
  .pipe(gulp.dest("build/font"));
});

gulp.task("copyimg", function () {
  return gulp.src([
    "src/img/**"
  ])
  .pipe(gulp.dest("build/img"));
});

gulp.task("copyjs", function () {
  return gulp.src([
    "src/js/libs/**"
  ])
  .pipe(gulp.dest("build/js/libs"));
});

//Запуск сборки
gulp.task("build", function (done) {
  run(
    "clean",
    "copyfont",
    "copyimg",
//    "images",
//    "webp",
    "copyjs",
    "style",
    "js",
//    "sprite",
//    "html",
    done
  );
});

gulp.task("serve", function() {
  server.init({
    server: "build/",
    notify: false,
    open: true,
    cors: true,
    ui: false
  });

  gulp.watch("src/style/**/*.sass", ["style"]).on("change", server.reload);
  gulp.watch("src/js/**/*.js", ["js"]).on("change", server.reload);
  gulp.watch("src/**/*.html", ["html"]).on("change", server.reload);
});


gulp.task('deploy', ['build'], function () {
    return gulp.src('build/css/**/*.*')
        .pipe(gulp.dest(config.destPath.src_css))
        .pipe(gulp.dest(config.destPath.tomcat_css));
});


// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(config.srcPath.sass, ['deploy']);
});

// For local as service
gulp.task('default', ['watch', 'deploy']);

// This task is exit after comleted.
// gulp.task('default', ['watch', 'deploy'], function() {
//     console.log('Task is done');
//     process.exit(0);
// });
